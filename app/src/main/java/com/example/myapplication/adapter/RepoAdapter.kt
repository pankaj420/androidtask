package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.databinding.RecyclerItemBinding
import com.example.myapplication.models.RepoResponse
import com.example.myapplication.viewholder.LoadingHolder
import com.example.myapplication.viewholder.RepoHolder
import io.realm.RealmRecyclerViewAdapter
import io.realm.RealmResults

class RepoAdapter(val data: RealmResults<RepoResponse>) :
    RealmRecyclerViewAdapter<RepoResponse, RecyclerView.ViewHolder>(data, true) {
    private var isLoading = false
    private val LOADING_VIEW = 1
    private val ITEM_VIEW = 2
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == ITEM_VIEW) {
            val binding: RecyclerItemBinding? = DataBindingUtil.bind(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycler_item, parent, false)
            )
            return RepoHolder(binding)
        }
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.loading_layout, parent, false)
        return LoadingHolder(view)

    }

    override fun getItemViewType(position: Int): Int {
        if (position < data.size)
            return ITEM_VIEW
        return LOADING_VIEW

    }

    override fun getItemCount(): Int {
        if (!isLoading)
            return data.size
        return data.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is RepoHolder)
            holder.onBind(data = data.get(position)!!)
    }


    fun setIsLoading(isLoading: Boolean) {
        this.isLoading = isLoading
        if (!isLoading)
            notifyItemRemoved(data.size + 1)
    }

    fun isLoading(): Boolean {
        return isLoading
    }
}