package com.example.myapplication.di

import com.example.myapplication.network.GithubService
import com.example.myapplication.viewmodel.MainViewModel
import io.realm.Realm
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {

//    single { RepoDb.getInstance(get()) }
//    factory { (get() as RepoDb).reposDao() }

    single { GithubService.create() }
    single {
        Realm.getDefaultInstance()
    }
    viewModel { MainViewModel(get(),get()) }
}
