package com.example.myapplication

import android.app.Application
import com.example.myapplication.di.appModule
import io.realm.Realm
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        startKoin {
            androidContext(this@MyApplication)
            modules(listOf(appModule))
        }
    }
}