package com.example.myapplication.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.RecyclerItemBinding
import com.example.myapplication.models.RepoResponse

class RepoHolder(val item: RecyclerItemBinding?) : RecyclerView.ViewHolder(item!!.root) {
    fun onBind(data: RepoResponse) {
        item?.repo = data
    }
}