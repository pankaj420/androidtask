package com.example.myapplication.network

import android.util.Log
import com.example.myapplication.models.RepoModel
import com.example.myapplication.models.RepoResponse

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val TAG = "GithubService"


fun fetchRepos(
    service: GithubService,
    page: Int,
    itemsPerPage: Int,
    onSuccess: (repos: List<RepoResponse>) -> Unit,
    onError: (error: String) -> Unit
) {
    service.searchRepos( page, itemsPerPage).enqueue(
        object : Callback<List<RepoResponse>> {
            override fun onFailure(call: Call<List<RepoResponse>>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<List<RepoResponse>>?,
                response: Response<List<RepoResponse>>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val repos = response.body()?: emptyList()
                    onSuccess(repos)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

interface GithubService {
    /**
     * fetch repos .
     */
    @GET("orgs/octokit/repos")
    fun searchRepos(
        @Query("page") page: Int,
        @Query("per_page") itemsPerPage: Int
    ): Call<List<RepoResponse>>

    companion object {
        private const val BASE_URL = "https://api.github.com/"

        fun create(): GithubService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubService::class.java)
        }
    }
}