package com.example.myapplication.viewmodel

import androidx.lifecycle.ViewModel
import com.example.myapplication.adapter.RepoAdapter
import com.example.myapplication.models.RepoResponse
import com.example.myapplication.network.GithubService
import com.example.myapplication.network.fetchRepos
import io.realm.Realm
import io.realm.RealmResults

class MainViewModel(val service: GithubService, val realm: Realm) : ViewModel() {
    val TAG = this.javaClass.name
    var pagecount = 0
    var isListFullyLoaded = false
    var repoList: RealmResults<RepoResponse>? = null
    fun getRepoList(adapter: RepoAdapter, page: Int, listner: (listFullLoaded: Boolean) -> Unit) {
        fetchRepos(
            service,
            page, 10, {
                if (it.size < 10)
                    listner(true)
                realm.apply {
                    beginTransaction()
                    insertOrUpdate(it)
                    commitTransaction()
                }
            }, {
                adapter.setIsLoading(false)
            }
        )
    }
}