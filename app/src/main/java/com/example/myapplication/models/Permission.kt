package com.example.myapplication.models

import io.realm.RealmObject

open class Permission(var admin: Boolean=false, var push: Boolean=false, var pull: Boolean=false) : RealmObject()