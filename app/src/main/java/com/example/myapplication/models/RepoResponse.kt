package com.example.myapplication.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class RepoResponse(
    @PrimaryKey var id: Int?=null, var open_issues_count: Int?=null, var open_issues: Int?=null, var license: License?=null,
    var permission: Permission?=null, var name: String?=null, var description: String?=null
) : RealmObject()