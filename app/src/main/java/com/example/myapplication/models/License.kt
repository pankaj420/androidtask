package com.example.myapplication.models

import io.realm.RealmObject

open  class License(
    var key: String?=null,
    var name: String?=null,
    var spdx_id: String?=null,
    var url: String?=null,
    var node_id: String?=null
) : RealmObject()