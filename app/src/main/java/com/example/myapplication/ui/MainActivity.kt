package com.example.myapplication.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapter.RepoAdapter
import com.example.myapplication.models.RepoResponse
import com.example.myapplication.viewmodel.MainViewModel
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {
    val viewModel: MainViewModel by viewModel()
    val realm: Realm by inject()
    val TAG = this.javaClass.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRecyclerView()
        rv_repo.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val currentPosition =
                    (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                val size = recyclerView.adapter?.itemCount
                val isLoading = (recyclerView.adapter as RepoAdapter).isLoading()
                if (currentPosition + 1 == size && !isLoading && !viewModel.isListFullyLoaded) {
                    (recyclerView.adapter as RepoAdapter).setIsLoading(true)
                    rv_repo.post {
                        (recyclerView.adapter as RepoAdapter).notifyItemInserted(viewModel.repoList!!.size + 1)
                    }
                    getNextPageRepo()
                }
            }
        })


    }

    fun initRecyclerView() {
        Log.d(TAG, "List fully loaded ${viewModel.isListFullyLoaded}")
        if (viewModel.repoList == null) {
            viewModel.repoList = realm.where(RepoResponse::class.java).findAll()
        }

        val layoutManager = LinearLayoutManager(this)
        rv_repo.layoutManager = layoutManager

        val adapter = RepoAdapter(viewModel.repoList!!)
        viewModel.repoList!!.addChangeListener { (_) ->
            adapter.setIsLoading(false)
        }
        rv_repo.adapter = adapter
        if (viewModel.repoList?.size == 0) {
            adapter.setIsLoading(true)
            adapter.notifyItemInserted(viewModel.repoList!!.size + 1)
            getNextPageRepo()
        }
    }

    fun getNextPageRepo() {
        viewModel.pagecount = (viewModel.repoList!!.size / 10) + 1
        Log.d(TAG, "Getting repo page ${viewModel.pagecount}")
        viewModel.getRepoList(
            rv_repo.adapter as RepoAdapter,
            viewModel.pagecount,
            { listFullLoaded ->
                viewModel.isListFullyLoaded = listFullLoaded
            })


    }
}
